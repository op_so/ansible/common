# `Ansible` collection Common

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/ansible/common/badges/main/pipeline.svg)](https://gitlab.com/op_so/ansible/common/pipelines)
[![semantic-release: angular](https://img.shields.io/badge/semantic--release-angular-e10079?logo=semantic-release)](https://github.com/semantic-release/semantic-release)

An [`Ansible`](https://www.ansible.com/) collection of common roles for Ubuntu and Debian Operating Systems.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/ansible/common) The main repository.

[![`Ansible` Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/common) `Ansible` Galaxy collection.

This collections includes:

| Roles                    | Description                                                  |
| ------------------------ | ------------------------------------------------------------ |
| [`in_dir`](#in_dir-role) | A role that downloads and installs a product in a directory. |
| [service](#service-role) | A role to create a `systemd` service with a dedicated user.  |

## `in_dir` role

A role that downloads and installs a product in a directory (/opt by default) with the following structure: `/<dir>/<product_name>/v<version>/<product_name>`.
Two symlinks are also created, if `in_dir_deploy` variable is `true` (default value):

- `/usr/local/bin/<product_name> -> /<dir>/<product_name>/current/<product_name>`
- `/<dir>/<product_name>/current/ -> /<dir>/<product_name>/v<version>/`

If a new product is deployed (change of `/<dir>/<product_name>/current/` symlink), the dictionary `in_dir_changed` is returned with a `true` value:
`in_dir_changed: {'<product_name>': true}`
otherwise the value in the dictionary is `false`.

### `in_dir` role variables

| Variables              | Description                                                                                                     | Default                     |
| ---------------------- | --------------------------------------------------------------------------------------------------------------- | --------------------------- |
| `in_dir_product`       | Name of the product. Example: `prometheus`                                                                      | **Required**                |
| `in_dir_version`       | Version of the product. Example: `2.40.1`                                                                       | **Required**                |
| `in_dir_package_name`  | Package name to download without extension. Example: `prometheus-2.40.1.linux-amd64`                            | **Required**                |
| `in_dir_package_ext`   | Define the type of compression. "" for no compression. Example: `tar.gz`                                        | **Required**                |
| `in_dir_download_link` | URL to download product. Example: `https://github.com/ ... /{{ in_dir_package_name }}.{{ in_dir_package_ext }}` | **Required**                |
| `in_dir_dir`           | The parent directory `<dir>` in which the package is installed.                                                 | `/opt`                      |
| `in_dir_package_dir`   | Directory of the extract files. Example: `.`                                                                    | `{{ in_dir_package_name }}` |
| `in_dir_deploy`        | Creation of 2 links to point to the new version.                                                                | `true`                      |
| `in_dir_temp_dir_keep` | If `true` the temp directory for download isn't deleted.                                                       | `false`                     |

### `in_dir` role output

If a new product is deployed (change of `/<dir>/<product_name>/current/` symlink), the dictionary `in_dir_changed` is returned with a `true` value:
`in_dir_changed: {'<product_name>': true}`
otherwise the value in the dictionary is `false`.

If a temp directory is created to download the product and `in_dir_temp_dir_keep` is set to `true`, the dictionary `in_dir_temp_dir_path` is returned with the path of the temp directory path:
`in_dir_temp_dir_path: {'<product_name>': '<temp_dir_path>'}`

## Service role

A role to create and configure a `systemd` service with an optional dedicated user.

### Service role variables

| Variables      | Description                                      | Default                              |
| -------------- | ------------------------------------------------ | ------------------------------------ |
| `service_name` | Name of the service. example: `node_exporter`    | **Required**                         |
| `service_user` | A dedicated user. To disable: `service_user: ""` | **Optional** Default: `service_name` |

### Service files

- `{{ playbook_dir }}/files/{{ service_name }}.service` or `{{ playbook_dir }}/templates/{{ service_name }}.service.j2`
- You must create a `systemd` service file with the name of the service in the `{{ playbook_dir }}/files` or `{{ playbook_dir }}/templates` directory.

## Getting Started

### Requirements

To use:

- Minimal `Ansible` version: 2.10

### Installation

- Download the `jfx.common` collection:

```shell
ansible-galaxy collection install jfx.common
```

- Then use the roles from the collection in the playbook:

example:

```yaml
   ...
    - role: jfx.common.in_dir
      vars:
        in_dir_product: prometheus
        in_dir_version: "{{ prometheus_version }}"
        in_dir_package_name: prometheus-{{ prometheus_version }}.linux-{{ arch }}
        in_dir_package_ext: tar.gz
        in_dir_download_link: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus_version }}/{{ in_dir_package_name }}.{{ in_dir_package_ext }}
        in_dir_deploy: false
        in_dir_temp_dir_keep: true

    - role: jfx.common.in_dir
    vars:
      in_dir_product: consul
      in_dir_version: "{{ consul_version }}"
      in_dir_package_name: "{{ in_dir_product }}_{{ in_dir_version }}_linux_{{ arch }}"
      in_dir_package_ext: zip
      in_dir_package_dir: "."
      in_dir_download_link: https://releases.hashicorp.com/{{ in_dir_product }}/{{ consul_version }}/{{ in_dir_package_name }}.{{ in_dir_package_ext }}
      in_dir_temp_dir_keep: true

    - role: jfx.common.service
      vars:
        service_name: prometheus

    - role: jfx.common.service
      vars:
        service_name: consul
```

## Authors

<!-- vale off -->
- **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
