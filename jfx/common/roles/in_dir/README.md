# Ansible in dir role

A role that downloads and installs a product in /opt directory.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/system) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Installation

* Download the `jfx.common` collection:

```shell
ansible-galaxy collection install jfx.common
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...

  roles:
    - role: jfx.common.in_dir
      vars:
        in_dir_product: prometheus
        in_dir_version: "{{ prometheus_version }}"
        in_dir_package_name: prometheus-{{ prometheus_version }}.linux-{{ arch }}
        in_dir_package_ext: tar.gz
        in_dir_download_link: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus_version }}/{{ in_dir_package_name }}.{{ in_dir_package_ext }}
        in_dir_deploy: false
        in_dir_temp_dir_keep: true

    - role: jfx.common.in_dir
    vars:
      in_dir_product: consul
      in_dir_version: "{{ consul_version }}"
      in_dir_package_name: "{{ in_dir_product }}_{{ in_dir_version }}_linux_{{ arch }}"
      in_dir_package_ext: zip
      in_dir_package_dir: "."
      in_dir_download_link: https://releases.hashicorp.com/{{ in_dir_product }}/{{ consul_version }}/{{ in_dir_package_name }}.{{ in_dir_package_ext }}
      in_dir_temp_dir_keep: true
```

### install_opt role variables

| Variables              | Description                                                                                                     | Default                     |
| ---------------------- | --------------------------------------------------------------------------------------------------------------- | --------------------------- |
| `in_dir_product`       | Name of the product. Example: `prometheus`                                                                      | **Required**                |
| `in_dir_version`       | Version of the product. Example: `2.40.1`                                                                       | **Required**                |
| `in_dir_package_name`  | Package name to download without extension. Example: `prometheus-2.40.1.linux-amd64`                            | **Required**                |
| `in_dir_package_ext`   | Define the type of compression. "" for no compression. Example: `tar.gz`                                        | **Required**                |
| `in_dir_download_link` | URL to download product. Example: `https://github.com/ ... /{{ in_dir_package_name }}.{{ in_dir_package_ext }}` | **Required**                |
| `in_dir_dir`           | The parent directory `<dir>` in which the package is installed.                                                 | `/opt`                      |
| `in_dir_package_dir`   | Directory of the extract files. Example: `.`                                                                    | `{{ in_dir_package_name }}` |
| `in_dir_deploy`        | Creation of 2 links to point to the new version.                                                                | `true`                      |
| `in_dir_temp_dir_keep` | If `true` the temp directory for download is not deleted.                                                       | `false`                     |

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
