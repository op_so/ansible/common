# Ansible service role

A role to create a systemd service with a dedicated user.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/system) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Installation

* Download the `jfx.common` collection:

```shell
ansible-galaxy collection install jfx.common
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...

  roles:
    - role: jfx.common.service
      vars:
        service_name: prometheus
```

### Service role variables

| Variables      | Description                                      | Default                              |
| -------------- | ------------------------------------------------ | ------------------------------------ |
| `service_name` | Name of the service. example: `node_exporter`    | **Required**                         |
| `service_user` | A dedicated user. To disable: `service_user: ""` | **Optional** Default: `service_name` |

### Service files

* `{{ playbook_dir }}/files/{{ service_name }}.service` or `{{ playbook_dir }}/templates/{{ service_name }}.service.j2`
* You must create a `systemd` service file with the name of the service in the `{{ playbook_dir }}/files` or `{{ playbook_dir }}/templates` directory.

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
